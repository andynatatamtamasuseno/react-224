import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { PropTypes } from 'prop-types'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import Checkbox from "@material-ui/core/CheckBox";
import { Edit, Delete } from "@material-ui/icons";
import { withStyles, styled } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
const useStyles = makeStyles(theme => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

class Form extends React.Component {
    
    createSelector =  (pageNum)=>{
        var selector=[];
        selector.push(<option value={1}> {1} </option>);
        for(var i=0;i<pageNum;i++){
        selector.push( <option key={1} value={i}>{i}</option>)
        }
        return selector;
    }
    render() {
        const classes= this.props;
        const { title, products, open, handleClose, onSelect, page, perpage, handleSelect, handleChange,pageNum } = this.props;
        return (
            <div>
                <Dialog
                    // fullScreen={fullScreen}
                    open={open}
                    onClose={handleSelect}
                    aria-labelledby="responsive-dialog-title">   

                    <FormControl className={classes.formControl}>
                        <InputLabel id="demo-simple-select-label">Page</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={page}
                            onChange={handleChange('page')}
                        >
                            {this.createSelector(pageNum)}
                        </Select>
                    </FormControl>
                    <DialogTitle id="responsive-dialog-title">List of Product
                    <FormControl className={classes.formControl}>
                        <InputLabel id="demo-simple-select-label">Page</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={page}
                            onChange={handleChange('page')}
                        >
                            {this.createSelector(pageNum)}
                        </Select>
                    </FormControl>
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText></DialogContentText>
                        <TableContainer component={Paper}>

                            <Table size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell >Select</TableCell>
                                        <TableCell >Code</TableCell>
                                        <TableCell >Initial</TableCell>
                                        <TableCell >Name</TableCell>
                                        <TableCell >Price</TableCell>
                                        <TableCell >Stock</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {products.map(detail => (
                                        <TableRow key={detail.Id}>

                                            <TableCell > <Button onClick={() => onSelect(detail.Id)} color="primary" variant="contained" autoFocus >+1</Button></TableCell>
                                            <TableCell >{detail.CodeProduct}</TableCell>
                                            <TableCell >{detail.Initial}</TableCell>
                                            <TableCell >{detail.Name}</TableCell>
                                            <TableCell >{detail.Price}</TableCell>
                                            <TableCell >{detail.Stock}</TableCell>



                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </DialogContent>
                    <Button onClick={handleClose} color="secondary" variant="contained" autoFocus>Close</Button>
                </Dialog>
            </div>

        )
    }
}


export default Form;