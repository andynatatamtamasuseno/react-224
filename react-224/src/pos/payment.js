import React from 'react';

import { PropTypes } from 'prop-types'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import Checkbox from "@material-ui/core/CheckBox";
import { Edit, Delete } from "@material-ui/icons";
import { withStyles, styled } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import { OrderService } from '../services/orderService';



class Payment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paid: 0
        }
        //     buttonPay:true,
        //     paid:'',
        //     reference:"",
        //     paymentGateway:{
        //         detaul:this.props.details
        //     },
        //     totalPayment: this.props.details.reduce((a,b) =>a +(parseFloat(b["Price"])* parseFloat(b["Quantity"])||0),0

        //     )
        // };


    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value
        })
    }

    onPayment = async () => {
        if (this.state.paid < this.state.totalPayment) {
            console.log("Failed Pay");
        }
        else {
            const { paymentGateway } = this.state;
            const payment = await OrderService.post(paymentGateway);
            if (payment.success) {
                this.state({
                    reference: payment.reference,
                    buttonPay: false
                })
            }
        }
    }
    render() {
        const { paid } = this.state;
        const { classes } = this.props;
        const { details, openPayment, handleClose, onPay, onSelect, openPay, reference } = this.props;
        const totalPayment = details.reduce((a,b)=>a +(parseFloat(b['Quantity'])* parseFloat(b['Price'])|| 0),0);
        var paidButton;
        if (totalPayment <= paid && reference.length == 0) {
            paidButton = <Button onClick={onPay} colour="primary" autofocus>Pay</Button>
        }
        return (
            <div>
                <Dialog
                    // fullScreen={fullScreen}
                    open={openPayment}
                    onClose={handleClose}
                    aria-labelledby="responsive-dialog-title"
                >   <Button onClick={handleClose} color="primary" variant="contained" autoFocus>
                        Close
                    </Button>
                    <DialogTitle id="responsive-dialog-title">Payment</DialogTitle>
                    <DialogContent>
                        <DialogContentText></DialogContentText>
                        <TableContainer component={Paper}>

                            <Table size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell >Product Name</TableCell>
                                        <TableCell >Product Price</TableCell>
                                        <TableCell >Quantity</TableCell>
                                        <TableCell >Amount</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {details.map(detail => (
                                        <TableRow key={detail.Id}>

                                            <TableCell>{detail.ProductName}</TableCell>
                                            <TableCell >{detail.Price}</TableCell>
                                            <TableCell >{detail.Quantity}</TableCell>
                                            <TableCell >
                                                {detail.Price * detail.Quantity}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                    <TableRow>
                                        <TableCell rowSpan={4} />
                                        <TableCell align="left" colSpan={2}>reference</TableCell>
                                        <TableCell align="center"><Input type="text" readOnly value={this.state.reference}></Input></TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell align="left" colSpan={2}>Total Payment</TableCell>
                                        <TableCell align="center"><Input type="number" readOnly value={totalPayment}></Input></TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell align="left">Paid</TableCell>
                                        <TableCell align="center"><Input type="number" onChange={this.handleChange} readOnly value={this.state.paid - totalPayment}></Input></TableCell>
                                    </TableRow>


                                </TableBody>
                            </Table>
                        </TableContainer>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary" autoFocus>{reference ? 'Close' : 'Cancle'}</Button>
                    </DialogActions>
                </Dialog>
            </div>

        )
    }
}


export default Payment;