import React from 'react';

import { PropTypes } from 'prop-types'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import Checkbox from "@material-ui/core/CheckBox";
import { Edit, Delete } from "@material-ui/icons";



import { ProductService } from '../services/productService';

import { OrderService } from '../services/orderService';
import Form from './form';
import Payment from './payment';
import { withStyles, styled } from '@material-ui/core';

class Pos extends React.Component {
    detailModel = {
        ProductCode: '',
        ProductName: '',
        Price: 0,
        Quantity: 0,
        Amount: 0
    }

    constructor(props) {
        super(props);
        this.state = {
            products: [],
            details: [],
            title: 'New Order',
            open: false,
            openPayment: false,
            reference: '',
            page:1,
            perpage:1,
            pageNum:2,
            filter:''
            
        }
    }

    onGetProduct = async ()=>{
        const{page,perpage,filter}= this.state;
        const res = await ProductService.getPage(page,perpage, filter);
        if(res.success){
            this.setState({
                products:res,
                pageNum:res.pageNum
            })
        }
        else{
            alert("Error"+res.result);
        }
    }

    loadProducts = async () => {
        const cats = await ProductService.getAll()
        // console.log(cats);
        if (cats.success) {
            this.setState({
                products: cats.result,
                pageNum: cats.pageNum

            });
        } else {
            alert('Error: ' + cats.result);
        }
    }
    componentDidMount() {
        this.loadProducts();
    }

    onCreate = () => {
        this.setState({
            title: 'Create',
            open: true,
            category: this.categoryModel
        })
    }

    onSelect = async (id) => {
        const entity = await ProductService.getById(id);
        console.log(entity);
        const { details } = this.state;
        const detail = {
            ProductCode: entity.result.CodeProduct,
            ProductName: entity.result.Name,
            Price: entity.result.Price,
            Quantity: 1
        }
        details.push(detail);
        this.setState({
            details: details,
            open: false
        })

    }
    onPayment=()=>{
        this.setState({
            openPayment:true
        })
    }

    onPay=async() =>{
        const req={
            Details : this.state.details
        }            
        const paid =await OrderService.post(req);

        if(paid.succes){
            this.setState({
                reference: paid.result.Reference
            })
        }
        else{
            alert('Error'+ paid.result);
        }


    }





    handleChange = (idx) => event => {
        if (parseInt(event.target.value) >= 0) {


            const { details } = this.state;
            details[idx].Quantity= event.target.value;
            this.setState({
                details: details
            })
        }

    }

    handleSelect =(name) =>event =>{
        this.setState({
            [name] : event.target.value
        })
    }

    onSearch = async(search)=>{
        const {page,perpage,filtering}= this.state; 
    }

    handleChangeCheckBox = name => event => {
        this.setState({
            category: {
                ...this.state.category,
                [name]: event.target.checked
            }
        })
    }

    handleSubmit = async () => {
        const { title, category } = this.state;
        // console.log(category);
        if (title === "Create") {
            const cat = await ProductService.post(category);
            if (cat.success) {
                this.setState({
                    category: this.categoryModel,
                    open: false
                })

                this.loadCategories();

            }
        }
        else if (title === "Edit") {

            const cat = await ProductService.put(category);
            if (cat.success) {
                this.setState({
                    category: this.categoryModel,
                    open: false

                })

                this.loadCategories();

            }
        }

        else if (title === 'Delete') {

            const cat = await ProductService.delete(category.Id);
            if (cat.success) {
                this.setState({
                    category: this.categoryModel,
                    open: false
                })

                this.loadCategories();

            }
        }
    }



    onDelete = async(idx)=>{
        const qz= window.confirm("Are you sure?")
        if(qz){
            const {details} = this.state;
            details.splice(idx,1);
        
        this.setState({
            openPay:true
        })}
    }

    onCancle = () =>
        this.setState({
            open: false,
            openPay: false
        })

    onNewOrder = () => {
        this.setState({
            open: true
        })
    }

    render() {
        const { products, details, title, open, openPay, reference ,page, perpage,pageNum } = this.state;
        const classes = this.props;
        return (

            <div>
                <Form title={title} products={products} open={open} handleChange={this.handleChange}page={page} perpage={perpage} onGetProduct={this.onGetProduct} pageNum={pageNum}
                    handleClose={this.onCancle}  onSelect={this.onSelect} />
                <Payment title={title} details={details} openPay={this.openPay} onPay={this.openPay}
                    handleClose={this.onCancle} onSelect={this.onSelect} reference={reference}  />
                <Button onClick={() => this.onNewOrder()} variant="contained" color="primary" >New Order</Button>
                
                <TableContainer component={Paper}>

                    <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell >Product</TableCell>
                                <TableCell >Price</TableCell>
                                <TableCell >Quantity</TableCell>
                                <TableCell >Amount</TableCell>
                                <TableCell >Remove</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {details.map((detail, Id) => (
                                <TableRow key={Id}>
                                    <TableCell > <Input type="text" value={detail.ProductCode} /></TableCell>
                                    <TableCell >{detail.Price}</TableCell>
                                    <TableCell > <Input type="number" value={detail.Quantity} onChange={this.handleChange(Id)} /></TableCell>
                                    <TableCell >{detail.Price * detail.Quantity}</TableCell>

                                    <TableCell>
                                        <Button onClick={() => this.onDelete(detail.Id)} variant="contained" color="primary"><Delete /></Button>
                                    </TableCell>

                                </TableRow>
                            ))}
                            <TableRow>
                                <TableCell>Grand Total</TableCell>
                                <TableCell></TableCell>
                                <TableCell>{details.reduce((a,b)=>a +(parseFloat(b['Quantity'])|| 0),0)}</TableCell>
                                <TableCell>{details.reduce((a,b)=>a +(parseFloat(b['Quantity'])* parseFloat(b['Price'])|| 0),0)}</TableCell>
                                <TableCell colSpan="4"><Button onClick={() => this.onPay()} variant="contained" color="primary">Payment</Button></TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>

        )
    }
}

const styles = theme => ({
    table: {
        minWidth: 650,
    }
})

Pos.proType = {
    classes: PropTypes.object.isRequired
}


export default withStyles(styles)(Pos);
