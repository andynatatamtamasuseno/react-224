import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from "@material-ui/core/CheckBox";
import { Edit, Delete } from "@material-ui/icons";


import { VariantService } from '../services/variantService';
import Form from './form';
import { CategoryService } from '../services/categoryService';

class Variant extends React.Component {
    variantModel = {
        id: 0,
        CodeVariants: '',
        CodeCategories: "",
        Initial: "",
        Name: "",
        Active: true
    }

    constructor(prom) {
        super(prom);
        this.state = {
            variants: [],
            categories:[],
            variant: this.variantModel,
            title: 'Create',
            open:false
        }
    }



    componentDidMount() {
        this.loadVariants();
        // this.loadCategories();
    }

    onCreate = () => {
        this.setState({
            title: 'Create',
            open: true,
            variant: this.variantModel
        })
    }
    // -----------------------------------------
    onEdit = async (id) => {
        const vat = await VariantService.getById(id);
        // const title= this.title;
        // console.log({ ...this.state.Variant });
        if (vat.success) {
            this.setState({
                variant: vat.result,
                title: 'Edit',
                open: true
            })
        }
        else {
            alert('Error' + vat.result);
        }

    }
    onDelete = async (id) => {
        const vat = await VariantService.getById(id);
        // console.log(vat);
        if (vat.success) {
            this.setState({
                variant: vat.result,
                title: 'Delete',
                open: true
            })
        }
        else {
            alert('Error' + vat.result);
        }

    }




    handleChange = name => event => {
        this.setState({
            variant: {
                ...this.state.variant,
                [name]: event.target.value
            }
        })
    }

    handleChangeCheckBox = name => event => {
        this.setState({
            variant: {
                ...this.state.variant,
                [name]: event.target.checked
            }
        })
    }

    handleSubmit = async () => {
        const { title, variant } = this.state;
        // console.log(variant);
        if (title === "Create") {
            const vat = await VariantService.post(variant);
            if (vat.success) {
                this.setState({
                    variant: this.variantModel,
                    open: false
                })

                this.loadVariants();

            }
        }
        else if (title === "Edit") {

            const vat = await VariantService.put(variant);
            if (vat.success) {
                this.setState({
                    variant: this.variantModel,
                    open: false
                })

                this.loadVariants();

            }
        }

        else if (title === 'Delete') {
            console.log(title ==='Delete');
            const vat = await VariantService.delete(variant.Id);
            if (vat.success) {
                this.setState({
                    variant: this.variantModel,
                    open:false
                })

                this.loadVariants();

            }
        }
    }

 

    loadVariants = async () => {
        const vats= await VariantService.getAll();
        const cats = await CategoryService.getAll();
        console.log(vats);
        if (vats.success) {
            this.setState({
                variants: vats.result,
                categories: cats.result
            });
        } else {
            alert('Error');
        }
    }

    onCancle =()=>
    this.setState({
        open: false
    })

    render() {
        const { variants, variant, title, categories, open } = this.state;
        return (
            <div>
                <Form title={title}  variant={variant} categories={categories} handleChange={this.handleChange} handleChangeCheckBox={this.handleChangeCheckBox} 
                handleSubmit={this.handleSubmit} handleClickOpen={this.onCreate} handleClose={this.onCancle} open={this.state.open}/>
                <TableContainer component={Paper}>
                    
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                {/* <TableCell>Dessert (100g serving)</TableCell> */}
                                <TableCell >Code Variants</TableCell>
                                <TableCell >Name Categories</TableCell>
                                <TableCell >Initial</TableCell>
                                <TableCell >Name</TableCell>
                                <TableCell >Active</TableCell>
                                <TableCell >Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {variants.map(row => (
                                <TableRow key={row.Id}>
                                   
                                    <TableCell >{row.CodeVariants}</TableCell>
                                    <TableCell >{row.CodeCategories}</TableCell>
                                    <TableCell >{row.Initial}</TableCell>
                                    <TableCell >{row.Name}</TableCell>
                                    <TableCell >
                                        <Checkbox disable checked={row.Active} />
                                    </TableCell>

                                    <TableCell>
                                        <Button onClick={() => this.onEdit(row.Id)} variant="contained" color="primary"><Edit /></Button>
                                        <Button onClick={() => this.onDelete(row.Id)} variant="contained" color="secondary"><Delete /></Button>
                                    </TableCell>

                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>

            // <div>
            //     <h3>List of Variants</h3>
            //     <table>
            //         <thead>
            //             <tr>
            //                 <th>CodeVariants</th>
            //                 <th>CodeCategories</th>
            //                 <th>Initial</th>
            //                 <th>Name</th>
            //                 <th>Active</th>
            //                 <th colSpan="2">Action</th>
            //             </tr>

            //         </thead>

            //         <tbody>
            //             {
            //                 variants.map(variant => {
            //                     return (
            //                         <tr key={variant.Id}>
            //                             <td>{variant.CodeVariants}</td>
            //                             <td>{variant.CodeCategories}</td>
            //                             <td>{variant.Initial}</td>
            //                             <td>{variant.Name}</td>
            //                             <td><input type="checkbox" checked={variant.Active}></input></td>
            //                             <td><input type="button" value="Edit" onClick={() => this.onEdit(variant.Id)} /></td>
            //                             <td><input type="button" value="Delete" onClick={() => this.onDelete(variant.Id)} /></td>

            //                         </tr>

            //                     )

            //                 })

            //             }
                        
            //         </tbody>

            //     </table>
            
                                
                            
            //     {/* {JSON.stringify(variant)} */}

            //     <br />
            //     <input type="button" value="Create New" onClick={this.onCreate} />


            //     <br />
            //     <label>{title}</label>

            //     <Form variant={variant} handleChangeCheckBox={this.handleChangeCheckBox} handleChange={this.handleChange}
            //     handleSubmit={this.handleSubmit} categories={this.state.categories} />

            // </div>
        )
    }

}
export default Variant;