import React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
// import {withStyles} from '@material-ui/core/styles';

import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import {PropTypes} from 'prop-types';
import {withStyles} from '@material-ui/core/styles';

import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';


import NativeSelect from '@material-ui/core/NativeSelect';





class Form extends React.Component {
    render() {
        const { title, variant, categories, handleChange, handleChangeCheckBox, handleSubmit, handleClickOpen, handleClose, open } = this.props;
        var formRO = (title === 'Delete' ? true : false);
        const codeRO = (title === 'Create' ? false : true)
        const{classes} = this.props;
        return (
            <div>
                <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                    Create New
        </Button>
                <Dialog
                    // fullScreen={fullScreen}
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>

                            <form className={classes.root} noValidate autoComplete="off">
                                <TextField id="standard-basic" label="CodeVariants" value={variant.CodeVariants}
                                    onChange={handleChange('CodeVariants')} inputProps={{ readOnly: codeRO }} disabl />
                                <br />
                                <InputLabel shrink htmlFor="age-native-label-placeholder">Code Category
                                </InputLabel>
                                <FormControl className={classes.formControl}>
                                <NativeSelect disabled={formRO}
                            
                                    value={variant.CodeCategories}
                                    onChange={handleChange('CodeCategories')}>
                                    <option value={variant.Code} onChange={handleChange('CodeCategories')} >
                                          {variant.CodeCategories ? variant.CodeCategories : "----Choose----"}

                                      </option>
                                    {
                                        
                                        categories.map(category => {
                                            return (
                                                <option value={category.Code} onChange={handleChange('CodeCategories')} >
                                                    {category.Name}

                                                </option>
                                            )
                                        })

                                    }
                                </NativeSelect>
                                </FormControl>
                                <br />
                                <TextField id="standard-basic" label="Initial" value={variant.Initial}
                                    onChange={handleChange('Initial')} inputProps={{ readOnly: formRO }} />
                                <br />
                                <TextField id="standard-basic" label="Name" value={variant.Name}
                                    onChange={handleChange('Name')} inputProps={{ readOnly: formRO }} />
                                <br />

                                <FormControlLabel
                                    disabled={formRO}
                                    value={variant.Active}
                                    control={<Checkbox color="primary" checked={variant.Active} onChange={handleChangeCheckBox('Active')} />}
                                    label="Active"
                                    labelPlacement="stark"

                                />
                            </form>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleSubmit} color="primary">
                            {title != "Delete" ? "Save" : "Delete"}
                        </Button>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            Close
            </Button>
                    </DialogActions>
                </Dialog>
            </div >
            // <div>
            //     <table>
            //         <tbody>
            //             <tr>

            //                 <td>
            //                     <label>Id</label>
            //                 </td>
            //                 <td>
            //                     <input type="text" value={variant.Id} onChange={handleChange('Id')} />
            //                 </td>
            //             </tr>

            //             <tr>

            //                 <td>
            //                     <label>CodeVariant</label>
            //                 </td>
            //                 <td>
            //                     <input type="text" value={variant.CodeVariants} onChange={handleChange('CodeVariants')} />
            //                 </td>
            //             </tr>
            //             <tr>

            //                 <td>
            //                     <label>CodeCategories</label>
            //                 </td>
            //                 <td>
            //                     <div>
            //                         <select>
            //                             <option value={variant.Code} onChange={handleChange('CodeCategories')} >
            //                                 {variant.CodeCategories ? variant.CodeCategories : "----Pilihan----"}

            //                             </option>
            //                             {

            //                                 categories.map(category => {
            //                                     return (
            //                                         <option value={variant.Code} onChange={handleChange('CodeCategories')} >
            //                                             {category.Code}

            //                                         </option>
            //                                     )
            //                                 })

            //                             }
            //                         </select>
            //                     </div>
            //                 </td>
            //             </tr>
            //             <tr>
            //                 <td>
            //                     <label>Initial</label>
            //                 </td>
            //                 <td>
            //                     <input type="text" value={variant.Initial} onChange={handleChange('Initial')} />
            //                 </td>
            //             </tr>
            //             <tr>
            //                 <td>
            //                     <label>Name</label>
            //                 </td>
            //                 <td>
            //                     <input type="text" value={variant.Name} onChange={handleChange('Name')} />
            //                 </td>
            //             </tr>
            //             <tr>
            //                 <td>
            //                     <label>Active</label>
            //                 </td>
            //                 <td>
            //                     <input type="checkbox" checked={variant.Active} onChange={handleChangeCheckBox('Active')} />
            //                 </td>
            //             </tr>
            //         </tbody>

            //         <tfoot>
            //             <tr>
            //                 <td colSpan="2"> <input type="button" value="submit" onClick={handleSubmit} /></td>
            //             </tr>

            //         </tfoot>
            //     </table>

            // </div>
        )
    }
}
const styles = theme =>({
    root:{
        '& > *':{
            margin:theme.spacing(1),
            width:200,
        },
    }
});


Form.propTypes={
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Form);