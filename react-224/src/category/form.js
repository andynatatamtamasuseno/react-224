import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';



class Form extends React.Component {

    render() {
        // const useStyles = makeStyles(theme => ({
        //     root: {
        //         '& > *': {
        //             margin: theme.spacing(1),
        //             width: 200,
        //         },
        //     },
        // }));
        // const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
        // const theme = useTheme();
        const { title, category, handleChange, handleChangeCheckBox, handleSubmit, handleClickOpen, handleClose, open } = this.props;
        var formRO= (title==='Delete'?true:false);
        const codeRO = (title==='Edit'||'Delete'? true: false)
        // const formROC= (title ===('Create')?true : false);
        // const classes = useStyles();
        return (
            <div>
                <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                    Create New
        </Button>
                <Dialog
                    // fullScreen={fullScreen}
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            
                            <form noValidate autoComplete="off">
                                <TextField id="standard-basic" label="Code" value={category.Code}
                                    onChange={handleChange('Code')} inputProps={{readOnly: codeRO}}
                                    
                                     />
                                    <br />
                                <TextField id="standard-basic" label="Initial" value={category.Initial}
                                    onChange={handleChange('Initial')} inputProps={{readOnly: formRO}}/>
                                    <br />
                                <TextField id="standard-basic" label="Name" value={category.Name}
                                    onChange={handleChange('Name')} inputProps={{readOnly: formRO}}/>
                                    <br/>

                                <FormControlLabel
                                    disabled={formRO}
                                    value={category.Active}
                                    control={<Checkbox color="primary" checked={category.Active}  onChange={handleChangeCheckBox('Active')}/>}
                                    label="Active"
                                    labelPlacement="stark"
                                    
                                />
                            </form>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleSubmit} color="primary">
                            {title!="Delete"?"Save":"Delete"}
            </Button>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            Close
            </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
        // return (
        //     <div>
        //         <table>
        //             <tbody>

        //                 <tr>

        //                     <td>
        //                         <label>Code</label>
        //                     </td>
        //                     <td>
        //                         <input type="text" value={category.Code} onChange={handleChange('Code')} />
        //                     </td>
        //                 </tr>
        //                 <tr>
        //                     <td>
        //                         <label>Initial</label>
        //                     </td>
        //                     <td>
        //                         <input type="text" value={category.Initial} onChange={handleChange('Initial')} />
        //                     </td>
        //                 </tr>
        //                 <tr>
        //                     <td>
        //                         <label>Name</label>                        </td>
        //                     <td>
        //                         <input type="text" value={category.Name} onChange={handleChange('Name')} />
        //                     </td>
        //                 </tr>
        //                 <tr>
        //                     <td>
        //                         <label>Active</label>
        //                     </td>
        //                     <td>
        //                         <input type="checkbox" checked={category.Active} onChange={handleChangeCheckBox('Active')} />
        //                     </td>
        //                 </tr>
        //             </tbody>

        //             <tfoot>
        //                 <tr>
        //                     <td colSpan="2"> <input type="button" value="submit" onClick={handleSubmit} /></td>
        //                 </tr>

        //             </tfoot>
        //         </table>
        //     </div>
        // )




    }
}

export default Form;