import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from "@material-ui/core/CheckBox";
import { Edit, Delete } from "@material-ui/icons";



import { CategoryService } from '../services/categoryService';
import Form from './form';



class Category extends React.Component {
    categoryModel = {
        id: 0,
        Code: '',
        Initial: '',
        Name: '',
        Active: true
    }


    constructor(promp) {
        super(promp);
        this.state = {
            categories: [],
            category: this.categoryModel,
            title: 'Create',
            open:false
        }


    }
    componentDidMount() {
        this.loadCategories();
    }

    onCreate = () => {
        this.setState({
            title: 'Create',
            open: true,
            category: this.categoryModel
        })
    }

    onEdit = async (id) => {
        const cat = await CategoryService.getById(id);
        // const title= this.title;
        console.log({ ...this.state.Category });
        if (cat.success) {
            this.setState({
                category: cat.result,
                title: 'Edit',
                open:true
            })
        }
        else {
            alert('Error' + cat.result);
        }

    }
    onDelete = async (id) => {
        const cat = await CategoryService.getById(id);
        // const title= this.title;
        // console.log({ ...this.state.Category });
        if (cat.success) {
            this.setState({
                category: cat.result,
                title: 'Delete',
                open:true
            })
        }
        else {
            alert('Error' + cat.result);
        }

    }



    handleChange = name => event => {
        this.setState({
            category: {
                ...this.state.category,
                [name]: event.target.value
            }
        })
    }

    handleChangeCheckBox = name => event => {
        this.setState({
            category: {
                ...this.state.category,
                [name]: event.target.checked
            }
        })
    }

    handleSubmit = async () => {
        const { title, category } = this.state;
        // console.log(category);
        if (title === "Create") {
            const cat = await CategoryService.post(category);
            if (cat.success) {
                this.setState({
                    category: this.categoryModel,
                    open:false
                })

                this.loadCategories();

            }
        }
        else if (title === "Edit") {

            const cat = await CategoryService.put(category);
            if (cat.success) {
                this.setState({
                    category: this.categoryModel,
                    open:false
                    
                })

                this.loadCategories();

            }
        }

        else if (title === 'Delete') {

            const cat = await CategoryService.delete(category.Id);
            if (cat.success) {
                this.setState({
                    category: this.categoryModel,
                    open:false
                })

                this.loadCategories();

            }
        }
    }



    loadCategories = async () => {
        const cats = await CategoryService.getAll()
        // console.log(cats);
        if (cats.success) {
            this.setState({
                categories: cats.result
            });
        } else {
            alert('Error');
        }
    }

    onCancle =()=>
    this.setState({
        open: false
    })
    render() {
        const { categories, category, title,open } = this.state;
        return (

            <div>
                <Form title={title} category={category} handleChange={this.handleChange} handleChangeCheckBox={this.handleChangeCheckBox} 
                handleSubmit={this.handleSubmit} handleClickOpen={this.onCreate} handleClose={this.onCancle} open={this.state.open}/>
                <TableContainer component={Paper}>
                    
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                {/* <TableCell>Dessert (100g serving)</TableCell> */}
                                <TableCell >Code Categories</TableCell>
                                <TableCell >Initial</TableCell>
                                <TableCell >Name</TableCell>
                                <TableCell >Active</TableCell>
                                <TableCell >Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {categories.map(row => (
                                <TableRow key={row.Id}>
                                   
                                    <TableCell >{row.Code}</TableCell>
                                    <TableCell >{row.Initial}</TableCell>
                                    <TableCell >{row.Name}</TableCell>
                                    <TableCell >
                                        <Checkbox disabled checked={row.Active} />
                                    </TableCell>

                                    <TableCell>
                                        <Button onClick={() => this.onEdit(row.Id)} variant="contained" color="primary"><Edit /></Button>
                                        <Button onClick={() => this.onDelete(row.Id)} variant="contained" color="secondary"><Delete /></Button>
                                    </TableCell>

                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
            // <div>
            //     <h3>List of Category</h3>

            //     <table>
            //         <thead>
            //             <tr>
            //                 <th>Id</th>
            //                 <th>Code</th>
            //                 <th>Initial</th>
            //                 <th>Name</th>
            //                 <th>Active</th>
            //                 <th> Action</th>
            //             </tr>

            //         </thead>

            //         <tbody>
            //             {
            //                 categories.map(category => {
            //                     return (
            //                         <tr key={category.Id}>
            //                             <td>{category.Id}</td>
            //                             <td>{category.Code}</td>
            //                             <td>{category.Initial}</td>
            //                             <td>{category.Name}</td>
            //                             <td><input type="checkbox" checked={category.Active}></input></td>
            //                             <td><input type="button" value="Edit" onClick={() => this.onEdit(category.Id)} /></td>
            //                             <td><input type="button" value="Delete" onClick={()=>this.onDelete(category.Id)} /></td>

            //                         </tr>

            //                     )

            //                 })

            //             }
            //         </tbody>

            //     </table>
            //     {/* {JSON.stringify(category)} */}

            //     <br />
            //     <input type="button" value="Create New" onClick={this.onCreate} />


            //     <br />
            //     <label>{title}</label>

            //     <Form category={category} handleChangeCheckBox={this.handleChangeCheckBox} handleChange={this.handleChange} handleSubmit={this.handleSubmit} />

            // </div>
        )
    }
}

export default Category;