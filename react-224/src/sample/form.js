import React from 'react';

class Form extends React.Component{
 render(){
    const {text, text2, handleChange} = this.props;
     return (
         <div>
             <label> Text</label>
             <input type="text" value={text} onChange={
             handleChange('text')} />
             <label> Text2</label>
             <input type="text" value={text2} onChange={
             handleChange('text2')} />
         </div>
         
     )
 }
}

export default Form;