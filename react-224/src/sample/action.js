import React from 'react';

class Action extends React.Component {
    render() {
        const { increment, decrement } = this.props;
        return (
            <div>
                {/* <button onClick={()=>alert ('Inc1')}>Increment 1</ button>
                 <input type="button" value="Ïncrement 2" onClick={()=>alert ('Inc2')}/> */}

                <button onClick={increment}>Increment 1</ button>
                <input type="button" value="Ïncrement 2" onClick={decrement} />
            </div>


        );
    }
}
export default Action;