import React from 'react';
import Add from './add';
import Action from './action';
import Form from './form';

class Sample extends React.Component {
    render() {
        const { val1, val2, valArr, increment, decrement,text,text2,
        handleChange } = this.props;
        return (
            <div>
                <h3>This is a Sample</h3>
                <h5>Value 1: {val1}</h5>
                <h5>Value 2: {val2}</h5>
                <Add val1={parseInt(val1)} val2={parseInt(val2)} valArr={valArr} />
                <Action increment={increment} decrement={decrement} />
                <Form 
                text={text} 
                text2={text2} 
                handleChange={handleChange} />
            </div>
        );

    }
}

export default Sample;