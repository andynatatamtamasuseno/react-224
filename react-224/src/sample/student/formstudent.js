import React from 'react';

class FormStudent extends React.Component{
 render(){
    const {student} = this.props;
     return (
         <div>
             <table>
                 <tr>
                     <td>Id</td>
                     <td>:</td>
                     <td><input type="text" value={student.id}/></td>
                 </tr>
                 <tr>
                     <td>First Name</td>
                     <td>:</td>
                     <td><input type="text" value={student.Firstname}/></td>
                 </tr>
                 <tr>
                     <td>Last Name</td>
                     <td>:</td>
                     <td><input type="text" value={student.LastName}/></td>
                 </tr>
                 <tr>
                     <td>Age</td>
                     <td>:</td>
                     <td><input type="text" value={student.Age} /></td>
                 </tr>
                 <tr>
                     <td>Active</td>
                     <td>:</td>
                     <td><input type="text" value={student.Active} /></td>
                 </tr>
                 
             </table>
         </div>
         
     )
 }
}

export default FormStudent;