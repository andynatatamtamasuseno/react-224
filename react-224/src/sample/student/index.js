import React from 'react';
import FormStudent from './formstudent';



class Student extends React.Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
        this.selectStudent = this.selectStudent.bind(this);

        
        this.state = {
            students: [
                { id: 1, Firstname: 'Andynata', LastName: 'Tamtama', Age: 23, Active: true },
                { id: 2, Firstname: 'Dimas', LastName: 'Santoso', Age: 20, Active: true },
                { id: 3, Firstname: 'Angga', LastName: 'Reiza', Age: 22, Active: false }
            ],
            student: {
                Id: 0,
                FirstName: '',
                LastName: '',
                Age: 0,
                Active: false
            }

        }
    }

    selectStudent = (id) => {
        const { students } = this.state;
        const student_temp = students.find(s => s.id === id);
        this.setState({
            student: student_temp
        })
    }


    handleChange(id) {
        const { students } = this.state;
        const student_temp = students.find(s => s.id === id);
        student_temp.Active= !student_temp.Active;
        this.setState({});
      }

   


    render() {
        const { students, student } = this.state;

        return (
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Firstname</th>
                            <th>LastName</th>
                            <th>Age</th>
                            <th>Active</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            students.map(student => {

                                return (
                                    <tr>
                                        <td>{student.Firstname}</td>
                                        <td>{student.LastName}</td>
                                        <td>{student.Age}</td>
                                        <td >{student.Active ? 'True' : 'False'}</td>
                                        <td>
                                        <input type="button" value="Read" onClick={() => this.selectStudent(student.id)} />
                                            <input type="checkbox" checked={student.Active} onClick={() =>this.handleChange(student.id)} />
                                        </td>
                                    </tr>

                                )
                            })
                        }
                    </tbody>

                </table>

                

                <FormStudent student={student} />

            </div>
        )
    }
}

export default Student;
