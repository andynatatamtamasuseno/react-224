import React from 'react';

class Add extends React.Component {
    render() {
        const { val1, val2, valArr } = this.props;
        return (
            <div>
                <h5>Add: {val1 + val2} </h5>
                <h5>ValArr:{JSON.stringify(valArr)}</h5>
            </div>


        );

    }
}

export default Add;