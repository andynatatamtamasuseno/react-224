import axios from 'axios';
import { config } from '../configurations/config';


export const VariantService = {
    getAll: () => {
        const result = axios.get(config.apiurl + '/variants')
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result;
    },

    getById: (id) =>{
        const result = axios.get (config.apiurl+ '/variants/' + id).then(response=>{
            return{
                success: response.data.success,
                result: response.data.result

            }
        })
            .catch(error =>{
                return{
                    success: false,
                    result: error
                }
            })
        return result;            
    },

    post: (variant)=>{
        const result = axios.post (config.apiurl +'/variants', variant).then(response => {
            return {
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error => {
            return{
                success: false,
                response: error
            }
        })
        return result;
    },

    put: (variant)=>{
        const result = axios.put (config.apiurl + '/variants/'+ variant.Id, variant).then(response => {
            return {
                success: response.data.success,
                result: response.data.message
            }
        })

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    },
    delete: id => {
        const result = axios.delete(config.apiurl + '/variants/' + id).then(response => {
            return {
                success: true,
                result: response
            }
        })

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    }
    

}