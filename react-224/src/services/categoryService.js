import axios from 'axios';
import { config } from '../configurations/config';

export const CategoryService = {
    getAll: () => {
        const result = axios.get(config.apiurl + '/categories')
            .then(respons => {
                // console.log(respons);
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result;
    },
    getById: (id) => {
        const result = axios.get(config.apiurl + '/categories/' + id).then(response => {
            return {
                success: response.data.success,
                result: response.data.result
            }
        })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result;
    },
    post: (category) => {
        console.log(category);
        const result = axios.post(config.apiurl + '/categories', category)
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    },

    put: (category) => {
        const result = axios.post(config.apiurl + '/categories/' + category.Id, category).then(response => {
            return {
                success: response.data.success,
                result: response.data.result
            }
        })

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    },
    delete: (id) => {
        const result = axios.post(config.apiurl + '/categories/' + id).then(response => {
            return {
                success: response.data.success,
                result: response.data.result
            }
        })
            

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    }


}