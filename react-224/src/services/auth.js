import axios from 'axios';
import { config } from '../configurations/config';

export const AuthService ={
    post: async (user)=>{
        const result = await axios.post(config.apiurl + '/login', user)
        .then(response => {
            localStorage.setItem('username', result.data.user.fullname)
            localStorage.setItem('username', result.data.user.username)
            localStorage.setItem('username', result.data.token)
            return {
                success: response.data.success,
                result: response.data.user
            }
        })

        .catch(error => {
            return {
                success: false,
                response: error
            }
        })
    return result;

    },

    getToken:()=>{
        return localStorage('token');
    },
    getUsername:()=>{
        return localStorage('username');
    },
    getFullname:()=>{
        return localStorage('fullname');
    }
}