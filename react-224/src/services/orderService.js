import axios from 'axios';
import { config } from '../configurations/config';

export const OrderService ={
    getById: (id) => {
        const result = axios.get(config.apiurl + '/order/' + id).then(response => {
            return {
                success: response.data.success,
                result: response.data.result
            }
        })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result;
    },

   

    post: (order) => {
        const result = axios.post(config.apiurl + '/order', order)
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    },
}