import axios from 'axios';
import { config } from '../configurations/config';

export const ProductService = {
    getAll: () => {
        const result = axios.get(config.apiurl + '/products')
            .then(respons => {
                // console.log(respons);
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result;
    },
    getById: (id) => {
        const result = axios.get(config.apiurl + '/products/' + id).then(response => {
            return {
                success: response.data.success,
                result: response.data.result
            }
        })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result;
    },
    post: (product) => {
        console.log(product);
        const result = axios.post(config.apiurl + '/products', product)
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    },

    getPage: (page,perpage,filtering) => {
        const result = axios.get(config.apiurl + `/products/${page}/${perpage}/${filtering}`).then(response => {
            return {
                success: response.data.success,
                pageNum: response.data.pageNum,
                result: response.data.result
            }
        })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result;
    },
    put: (product) => {
        const result = axios.post(config.apiurl + '/products/' + product.Id, product).then(response => {
            return {
                success: response.data.success,
                result: response.data.result
            }
        })

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    },
    delete: (id) => {
        const result = axios.post(config.apiurl + '/products/' + id).then(response => {
            return {
                success: response.data.success,
                result: response.data.result
            }
        })
            

            .catch(error => {
                return {
                    success: false,
                    response: error
                }
            })
        return result;
    }


}