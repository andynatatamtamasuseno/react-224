import React from 'react';
import './App.css';
import PropTypes from 'prop-types'
import{withStyles} from '@material-ui/core/styles'
import {ThemeProvider as MuiThemeProvider} from '@material-ui/core/styles'
import Category from './category';
// import Sample from './sample/index';
// import Students from './sample/student';

import Sidebar from './layout/sidebar';
import {ThemeProvider as MuiThemeProvider} from '@material-ui/core/styles';
import { BrowserRouter} from 'react-router-dom'

import Variant from './variant';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
     logged:false
     
    }
  }

  handleChangeStatus = (value) =>{
    this.setState({
      logged:value
    })
  }


  // increment = () => {
  //   this.setState({
  //     val2: this.state.val2 + 3
  //   })
  // }

  // decrement = () => {
  //   const { valArr } = this.state;
  //   valArr.val3 = valArr.val3 - 2;
  //   this.setState({
  //     valArr: valArr
  //   })
  // }

  // handleChange = name => event =>{
  //   this.setState({
  //     [name]: event.target.value
  //   });
  // }

  render(){
    const {logged}= this.state;
    const theme = this.props;
    return(
      <div>
        {/* <Students /> */}
        {/* <Category /> */}
        {/* <Variant /> */}
        <MuiThemeProvider theme={theme}>
          <BrowserRouter>
          <Sidebar logged={logged} handleChangeStatus={this.handleChangeStatus}/>
          <Sidebar />
          </BrowserRouter>
        </MuiThemeProvider>
        
      </div>
    )
  }

  // render() {
  //   const { val1, val2, valArr,text, text2 } = this.state;
  //   return (
  //     <div>
  // <h5> {text+ ' '+text2}</h5>
  //       <Sample val1={val1} val2={val2} valArr={valArr}
  //         increment={this.increment}
  //         decrement={this.decrement} text={text} text2={text2}
  //         handleChange={this.handleChange} />
  //     </div>
  //   )
  // }
}
const styles = theme => ({});
App.PropTypes={
  classes: PropTypes.object.isRequired
}

export default  withStyles(styles,{withTheme:true})(App);
