import React from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from "@material-ui/core/CheckBox";
import { Edit, Delete } from "@material-ui/icons";



import { ProductService } from '../services/productService';
import Form from './form';



class product extends React.Component {
    productModel = {
        id: 0,
        CodeProduct: '',
        Initial: '',
        Name: '',
        Description: '',
        Price: 0,
        Active: true,
        Stock: 0

    }


    constructor(promp) {
        super(promp);
        this.state = {
            products: [],
            product: this.productModel,
            title: 'Create',
            open: false
        }


    }
    componentDidMount() {
        this.loadproducts();
    }

    onCreate = () => {
        this.setState({
            title: 'Create',
            open: true,
            product: this.productModel
        })
    }

    onEdit = async (id) => {
        const cat = await ProductService.getById(id);
        console.log({ ...this.state.product });
        if (cat.success) {
            this.setState({
                product: cat.result,
                title: 'Edit',
                open: true
            })
        }
        else {
            alert('Error' + cat.result);
        }

    }
    onDelete = async (id) => {
        const cat = await ProductService.getById(id);
        if (cat.success) {
            this.setState({
                product: cat.result,
                title: 'Delete',
                open: true
            })
        }
        else {
            alert('Error' + cat.result);
        }

    }



    handleChange = name => event => {
        this.setState({
            product: {
                ...this.state.product,
                [name]: event.target.value
            }
        })
    }

    handleChangeCheckBox = name => event => {
        this.setState({
            product: {
                ...this.state.product,
                [name]: event.target.checked
            }
        })
    }

    handleSubmit = async () => {
        const { title, product } = this.state;
        if (title === "Create") {
            const cat = await ProductService.post(product);
            if (cat.success) {
                this.setState({
                    product: this.productModel,
                    open: false
                })

                this.loadproducts();

            }
        }
        else if (title === "Edit") {

            const cat = await ProductService.put(product);
            if (cat.success) {
                this.setState({
                    product: this.productModel,
                    open: false

                })

                this.loadproducts();

            }
        }

        else if (title === 'Delete') {

            const cat = await ProductService.delete(product.Id);
            if (cat.success) {
                this.setState({
                    product: this.productModel,
                    open: false
                })

                this.loadproducts();

            }
        }
    }



    loadproducts = async () => {
        const cats = await ProductService.getAll()
        if (cats.success) {
            this.setState({
                products: cats.result
            });
        } else {
            alert('Error');
        }
    }

    onCancle = () =>
        this.setState({
            open: false
        })
    render() {
        const { products, product, title, open } = this.state;
        return (

            <div>
                <Form title={title} product={product} handleChange={this.handleChange} handleChangeCheckBox={this.handleChangeCheckBox}
                    handleSubmit={this.handleSubmit} handleClickOpen={this.onCreate} handleClose={this.onCancle} open={this.state.open} />
                <TableContainer component={Paper}>

                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell >Code products</TableCell>
                                <TableCell >Initial</TableCell>
                                <TableCell >Name</TableCell>
                                <TableCell >Description</TableCell>
                                <TableCell >Price</TableCell>
                                <TableCell >Active</TableCell>
                                <TableCell >Stock</TableCell>
                                <TableCell >Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {products.map(row => (
                                <TableRow key={row.Id}>

                                    <TableCell >{row.CodeProduct}</TableCell>
                                    <TableCell >{row.Initial}</TableCell>
                                    <TableCell >{row.Name}</TableCell>
                                    <TableCell >{row.Description}</TableCell>
                                    <TableCell >{row.Price}</TableCell>
                                    <TableCell >
                                        <Checkbox disabled checked={row.Active} />
                                    </TableCell>
                                    <TableCell >{row.Stock}</TableCell>

                                    <TableCell>
                                        <Button onClick={() => this.onEdit(row.Id)} variant="contained" color="primary"><Edit /></Button>
                                        <Button onClick={() => this.onDelete(row.Id)} variant="contained" color="secondary"><Delete /></Button>
                                    </TableCell>

                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
            
        )
    }
}

export default product;