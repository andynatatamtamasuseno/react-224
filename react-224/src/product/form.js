import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';




class Form extends React.Component {

    render() {

        const { title, product, handleChange, handleChangeCheckBox, handleSubmit, handleClickOpen, handleClose, open } = this.props;
        var formRO = (title === 'Delete' ? true : false);
        const codeRO = (title === 'Edit' || 'Delete' ? true : false)
        return (
            <div>
                <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                    Create New
        </Button>
                <Dialog
                    // fullScreen={fullScreen}
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>

                            <form noValidate autoComplete="off">
                                <TextField id="standard-basic" label="Code" value={product.Code}
                                    onChange={handleChange('Code')} inputProps={{ readOnly: codeRO }}

                                />
                                <br />
                                <TextField id="standard-basic" label="Initial" value={product.Initial}
                                    onChange={handleChange('Initial')} inputProps={{ readOnly: formRO }} />

                                <br />
                                <TextField id="standard-basic" label="Name" value={product.Name}
                                    onChange={handleChange('Name')} inputProps={{ readOnly: formRO }} />

                                <br />
                                <TextField id="standard-basic" label="Description" value={product.Description}
                                    onChange={handleChange('Description')} inputProps={{ readOnly: formRO }} />

                                <br />
                                <TextField id="standard-basic" label="Price" value={product.Price}
                                    onChange={handleChange('Price')} inputProps={{ readOnly: formRO }} />
                                <br />

                                <FormControlLabel
                                    disabled={formRO}
                                    value={product.Active}
                                    control={<Checkbox color="primary" checked={product.Active} onChange={handleChangeCheckBox('Active')} />}
                                    label="Active"
                                    labelPlacement="stark"

                                />

                                <br />
                                <TextField id="standard-basic" label="Stock" value={product.Stock}
                                    onChange={handleChange('Stock')} inputProps={{ readOnly: formRO }} />
                                <br />
                            </form>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleSubmit} color="primary">
                            {title != "Delete" ? "Save" : "Delete"}
                        </Button>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            Close
            </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )


    }
}

export default Form;